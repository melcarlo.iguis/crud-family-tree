const User = require('../models/User');


module.exports.createUser = (reqBody) =>{

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age : reqBody.age,
		birthdate: reqBody.birthdate

	})

	return newUser.save().then((user, error)=>{
		if(error){
			console.log("failed to register")
			return false
		}else{
			console.log("successfully register")
			return true

		}
	})
}



module.exports.getAllusers = () => {
	return User.find({}).then(result =>{
		return result
	})
}

// -  Update user by id
module.exports.updateUser = (reqParams, reqBody) =>{

	let updateUserInfo = {

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age : reqBody.age,
		birthdate: reqBody.birthdate
	}


	return User.findByIdAndUpdate(reqParams.userId, updateUserInfo).then((update,error) =>{
		if(error){
			console.log('Error occur')
			return false
		}else{
			console.log('user updated successfully')
			return true
		}
	})
}


// Delete user

module.exports.deleteUser = (reqParams) => {
	return User.findByIdAndDelete(reqParams.userId).then((del,error) => {
		if(error){
			console.log("deleting user failed")
			return false
		}else{
			console.log("successfully deleted a user")
			return true
		}
	})
}


// family member details
module.exports.familyDetails = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return User.find({lastName: result.lastName}).then(data=> {
			return data
		})
	})
}
