const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({

	firstName : {
		type: String,
		required : [true, 'First name is required']
	},
	lastName : {
		type: String,
		required : [true, 'Last Name is required']
	},

	age : {
		type: String,
		required : [true, 'age is required']
	},

	birthdate : {
		type: Date,
		required : [true, 'age is required']
	}
	

	
})

module.exports = mongoose.model('User', userSchema)