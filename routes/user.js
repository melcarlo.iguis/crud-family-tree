const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');

// register or create user routes
router.post('/create' , (req,res) => {
	userController.createUser(req.body).then(resultFromController => res.send(resultFromController))
})

// retrieve all user
router.get('/all', (req, res)=>{
	userController.getAllusers().then(resultFromController =>res.send(resultFromController))
})

// -  Update User information
router.put('/:userId/update' , (req, res) => {
	userController.updateUser(req.params, req.body).then(resultFromController=> res.send(resultFromController))
})

// - Delete a user
router.delete('/:userId/delete', (req, res) => {
	userController.deleteUser(req.params).then(resultFromController=> res.send(resultFromController))
})


// - Rertriving all family member info
router.get('/:userId/familyDetail', (req, res)=> {
	userController.familyDetails(req.params).then(
		resultFromController=> res.send(resultFromController))
})
module.exports = router;